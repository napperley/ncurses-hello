package org.example.ncursesHello

import ncurses.endwin
import ncurses.getch
import ncurses.initscr
import ncurses.mvprintw

fun main() {
    initscr()
    mvprintw(2, 5, "Hello World! :)")
    mvprintw(3, 5, "Press any key to exit.")
    getch()
    endwin()
}
