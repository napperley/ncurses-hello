group = "org.example"
version = "1.0-SNAPSHOT"

plugins {
    kotlin("multiplatform") version "1.3.70"
}

kotlin {
    linuxX64 {
        compilations.getByName("main") {
            cinterops.create("ncurses_x64") {
                val ncursesDir = "${System.getenv("HOME")}/ncurses-6.1"
                val includeDir = "$ncursesDir/include"
                compilerOpts(includeDir)
                extraOpts("-libraryPath", "$ncursesDir/lib")
                includeDirs(includeDir)
            }
        }
        binaries {
            executable("ncurses_hello") {
                entryPoint = "org.example.ncursesHello.main"
            }
        }
    }
}